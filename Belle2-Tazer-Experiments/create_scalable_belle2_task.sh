#/bin/bash
module load python/anaconda3.2019.3
scriptDir=`pwd`
belle2_trace_dir=${scriptDir}/Belle2TracesforTazer/

ioratio=$1
iorate=$(( 125*ioratio ))

tpf=$2
numTasks=$3
cores_per_node=$4
nodes=$5
numCores=$((cores_per_node * nodes))
tazer_servers=$6
use_local_server=$7
# ${ioratio} ${tpf} ${numTasks} ${cores_per_node} ${nodes} ${tazer_server_nodes} ${use_local_server} ${tlimit} ${numCycles} ${readProbability} ${random_access} ${scalable} ${private_size} ${shared_mem} ${shared_size}
scalable=${11}
private_size=${12}
shared_mem=${13}
shared_size=${14}

echo "scalable:${scalable} private size:${private_size}MB shared:${shared_mem} sh size:${shared_size}GB"


coreIoRate=6.7 # this is derived from an actual belle2 run

if (( use_local_server < 3 )); then
    use_bounded_filelock=1
else
    use_bounded_filelock=0
fi
use_bounded_filelock=0
echo "use_local_server: ${use_local_server} use_bounded_filelock: ${use_bounded_filelock}"

cat <<EOT > tasks/mc_tazer.sh
#!/bin/bash
MY_HOSTNAME=\`hostname\`
ulimit -n 4096

TAZER_PATH=${TAZER_BUILD_DIR}/src/client
echo "\$JOBID"
TASKDIR="./"
mkdir -p \$TASKDIR\$JOBID
cp send_task_msg.py \$TASKDIR\$JOBID
cp ParseTazerOutput.py \$TASKDIR\$JOBID
cd \$TASKDIR\$JOBID

loop=\`python send_task_msg.py "6=\$MY_HOSTNAME=\$JOBID"\`
while [ "\$loop" == "0" ] ; do
    start=\$(date +%s)
    end=\$(date +%s)
    tdiff=\$(( end - start ))
    twait=\`shuf -i 10-30 -n 1\`
    while [ \$tdiff -le \$twait ]; do
        end=\$(date +%s)
        tdiff=\$(( end - start ))
        #echo "\$start \$end \$tdiff"
    done
    echo  "checking to start..."
    loop=\`python send_task_msg.py "6=\$MY_HOSTNAME=\$JOBID"\`
done;

N=1
expName="tazer"
taskType="tazer"
ioType="tazer"

t=\$(date +%s)
var_names="StartTime" && var_vals="\${t}" && var_times="\${t}"
var_names="\${var_names},N" && var_vals="\${var_vals},\${N}" && var_times="\${var_times},\${t}"
var_names="\${var_names},ExpName" && var_vals="\${var_vals},\${expName}" && var_times="\${var_times},\${t}"
var_names="\${var_names},TaskType" && var_vals="\${var_vals},\${taskType}" && var_times="\${var_times},\${t}"
var_names="\${var_names},Host" && var_vals="\${var_vals},\${MY_HOSTNAME}" && var_times="\${var_times},\${t}"
var_names="\${var_names},IOType" && var_vals="\${var_vals},\${ioType}" && var_times="\${var_times},\${t}"
var_names="\${var_names},Slot" && var_vals="\${var_vals},\${SLOT}" && var_times="\${var_times},\${t}"

WORKDIR=\`pwd\`
echo "### Working directory on ###"
echo \$WORKDIR



echo "### Running on ###"
hostname
tazer_lib=\${TAZER_PATH}/libclient.so 
out_dir=./
data_dir=./
mkdir -p \$out_dir
mkdir -p \$data_dir


module load gcc/8.1.0  

cd \$WORKDIR
echo "### Working directory on ###"
echo \$WORKDIR
ls

echo "### Creating tazer meta files"


compression=0
blocksize=\$(( 1024*1024 )) #16777216


dset=\$(( \${MYID} / ${tpf} ))
t=\$(date +%s)
var_names="\${var_names},InputDataSet" && var_vals="\${var_vals},\${dset}" && var_times="\${var_times},\${t}"
files="BHWide_ECL-phase3-optimized.root Coulomb_HER_ECL-phase3-optimized.root RBB_ECL-phase3-optimized.root Touschek_LER_ECL-phase3-optimized.root \
BHWideLargeAngle_ECL-phase3-optimized.root Coulomb_HER_PXD-phase3-optimized.root RBB_PXD-phase3-optimized.root Touschek_LER_PXD-phase3-optimized.root \
BHWideLargeAngle_PXD-phase3-optimized.root Coulomb_HER_usual-phase3-optimized.root RBB_usual-phase3-optimized.root Touschek_LER_usual-phase3-optimized.root \
BHWideLargeAngle_usual-phase3-optimized.root Coulomb_LER_ECL-phase3-optimized.root Touschek_HER_ECL-phase3-optimized.root twoPhoton_ECL-phase3-optimized.root \
BHWide_PXD-phase3-optimized.root Coulomb_LER_PXD-phase3-optimized.root Touschek_HER_PXD-phase3-optimized.root twoPhoton_PXD-phase3-optimized.root \
BHWide_usual-phase3-optimized.root Coulomb_LER_usual-phase3-optimized.root Touschek_HER_usual-phase3-optimized.root twoPhoton_usual-phase3-optimized.root"

file_paths="" 


for file in \$files; do
    #every client looks at set1 folder so we have intra-task reuse 
    #infile=/home/frie869/ippd_data/set\${dset}/\${file} #assuming server is on seapearl
    infile=/files0/frie869/ippd_data/set1/\${file} #assuming server is on bluesky, if we use set\$\{dset\} we won't have intertask reuse on shared every task would have their unique files 
    if [ "${use_local_server}" == "0" ]; then
        server="130.20.68.151" 
        echo "\${server}:5101:\${compression}:0:0:\${blocksize}:\${infile}|\${server}:5201:\${compression}:0:0:${blocksize}:\${infile}|\${server}:5301:\${compression}:0:0:\${blocksize}:\${infile}|\${server}:5401:\${compression}:0:0:\${blocksize}:\${infile}|" | tee \$data_dir/\${file}.meta.in
    else
        server_list=""
        if [ "${use_local_server}" == "1" ] || [ "${use_local_server}" == "3" ]; then
            echo "TAZER0.1" > \$data_dir/\${file}.meta.in
            echo "type=input" >>\$data_dir/\${file}.meta.in
            for server in `python ParseSlurmNodelist.py $tazer_servers`; do
                echo "[server]" >> \$data_dir/\${file}.meta.in
                echo "host=\${server}" >>\$data_dir/\${file}.meta.in
                echo "port=5001" >> \$data_dir/\${file}.meta.in
                echo "file=\${infile}" >>\$data_dir/\${file}.meta.in
            done
        else
            for server in `python ParseSlurmNodelist.py $tazer_servers`; do
                server_list="\${server_list}\${server}.ibnet:5001:\${compression}:0:0:\${blocksize}:\${infile}|\${server}:5001:\${compression}:0:0:\${blocksize}:\${infile}|"
            done
            echo "\${server_list}" | tee \$data_dir/\${file}.meta.in
        fi
    fi

    cat \$data_dir/\${file}.meta.in
    file_paths="\$file_paths \$data_dir/\${file}.meta.in "
done

echo "TAZER0.1" > \${out_dir}/\${JOBID}.root.meta.out
echo "type=output" >>\${out_dir}/\${JOBID}.root.meta.out
for server in `python ParseSlurmNodelist.py $tazer_servers`; do
    echo "[server]" >> \${out_dir}/\${JOBID}.root.meta.out
    echo "host=\${server}" >>\${out_dir}/\${JOBID}.root.meta.out
    echo "port=5001" >> \${out_dir}/\${JOBID}.root.meta.out
    #echo "file=output/\${JOBID}.root" >>\${out_dir}/\${JOBID}.root.meta.out
done

file_paths="\$file_paths  \$out_dir/\${JOBID}.root.meta.out"

echo "### Copying input files ###"
t=\$(date +%s)
var_names="\${var_names},StartInputTx" && var_vals="\${var_vals},\${t}" && var_times="\${var_times},\${t}"
t=\$(date +%s)
var_names="\${var_names},StopInputTx" && var_vals="\${var_vals},\${t}" && var_times="\${var_times},\${t}"


echo "## Running belle2 scalable tests##"
outfile=workload_sim_\${JOBID}.txt

t=\$(date +%s)
var_names="\${var_names},StartExp" && var_vals="\${var_vals},\${t}" && var_times="\${var_times},\${t}"

TAZER_SCALABLE_CACHE=${scalable}
TAZER_SCALABLE_CACHE_NUM_BLOCKS=${private_size}
TAZER_PRIVATE_MEM_CACHE_SIZE=$((private_size*1024*1024))
TAZER_BLOCKSIZE=$((1*32*1024))
TAZER_SHARED_MEM_CACHE=${shared_mem}
TAZER_SHARED_MEM_CACHE_SIZE=$((shared_size*1024*1024*1024))
TAZER_SCALABLE_CACHE_ALLOCATOR=0
TAZER_SHARED_MEM_CACHE=1
TAZER_BB_CACHE=0
TAZER_FILE_CACHE=0
TAZER_BOUNDED_FILELOCK_CACHE=0
TAZER_FILELOCK_CACHE=0
TAZER_TRACE_HISTOGRAM=0
TAZER_Hb_VALUE=10
TAZER_H_VALUE=1
TAZER_MC_VALUE=0
TAZER_Sr_VALUE=1
TAZER_UMB_THRESHOLD="0.0001"


# TAZER_BB_CACHE_SIZE=\$(( 195*1024*1024*1024 ))
# TAZER_BOUNDED_FILELOCK_CACHE_SIZE=\$(( 512*1024*1024*1024 ))

echo "sizes: \${TAZER_PRIVATE_MEM_CACHE_SIZE} \${TAZER_SHARED_MEM_CACHE_SIZE} \${TAZER_BB_CACHE_SIZE} \${TAZER_BOUNDED_FILELOCK_CACHE_SIZE}"

echo `pwd`
time TAZER_PREFETCH=0 \
TAZER_SCALABLE_CACHE=\${TAZER_SCALABLE_CACHE} TAZER_SCALABLE_CACHE_NUM_BLOCKS=\${TAZER_SCALABLE_CACHE_NUM_BLOCKS} TAZER_PRIVATE_MEM_CACHE_SIZE=$((private_size*1024*1024)) \
TAZER_SHARED_MEM_CACHE=\${TAZER_SHARED_MEM_CACHE} TAZER_SHARED_MEM_CACHE_SIZE=\${TAZER_SHARED_MEM_CACHE_SIZE} \
TAZER_BOUNDED_FILELOCK_CACHE=${use_bounded_filelock} TAZER_BOUNDED_FILELOCK_CACHE_SIZE=\${TAZER_BOUNDED_FILELOCK_CACHE_SIZE} \
TAZER_Hb_VALUE=\${TAZER_Hb_VALUE} TAZER_H_VALUE=\${TAZER_H_VALUE} TAZER_MC_VALUE=\${TAZER_MC_VALUE} TAZER_Sr_VALUE=\${TAZER_Sr_VALUE} \
TAZER_UMB_THRESHOLD=\${TAZER_UMB_THRESHOLD} TAZER_BLOCKSIZE=\${TAZER_BLOCKSIZE} \
LD_PRELOAD=\${preload}:\${tazer_lib} ${scriptDir}/workloadSim -f ${belle2_trace_dir}/Belle2Trace_set\${dset}.txt -i ${coreIoRate} -m ".meta.in" -o ".meta.out" -t ${timelimit} >& \${out_dir}\${outfile}
#gdb -ex run --ex "thread apply all bt" --args env LD_PRELOAD=\${preload}:\${tazer_lib} ${scriptDir}/workloadSim -f ${belle2_trace_dir}/Belle2Trace_set\${dset}.txt -i ${coreIoRate} -m ".meta.in" -o ".meta.out" -t ${timelimit} >& \${out_dir}\${outfile}

t=\$(date +%s)
var_names="\${var_names},StopExp" && var_vals="\${var_vals},\${t}" && var_times="\${var_times},\${t}"

parsed=\`python ParseTazerOutput.py \${out_dir}\${outfile}\`
tmp_names=\`echo "\$parsed" | grep -oP '(?<=labels:).*'\` 
tmp_vals=\`echo "\$parsed" | grep -oP '(?<=vals:).*'\` 
var_names="\${var_names},\${tmp_names}" && var_vals="\${var_vals},\${tmp_vals}"

#rm -r \$data_dir

t=\$(date +%s)
var_names="\${var_names},FinishedTime" && var_vals="\${var_vals},\${t}" && var_times="\${var_times},\${t}"
python send_task_msg.py "2=\$MY_HOSTNAME=\$JOBID=\$JOBID;\$var_names;\$var_vals"
python send_task_msg.py "1=\$MY_HOSTNAME=\$JOBID"
wait
EOT

# done

echo "creating task list "
touch CurTasks.dat
for i in `seq 0 $(( numTasks-1 ))`; do
# echo "${iorate}_${tpf}_$(( i/tpf )).sh" >> CurTasks.dat
#echo "${iorate}_${tpf}.sh" >> CurTasks.dat
echo "mc_tazer.sh" >> CurTasks.dat

# for i in `seq 1 ${cores_per_node}`; do
# echo "${iorate}_${tpf}.sh" >> CurTasks.dat
done
module unload python/anaconda3.2019.3
