import numpy as np
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--inputFileName", type=str,
                        help="name/path of file to generate accesses for", default="accesses.txt")

    args = parser.parse_args()
    data = np.genfromtxt(args.inputFileName, usecols=(1))

    max_read =int(np.max(data))
    print (max_read)
