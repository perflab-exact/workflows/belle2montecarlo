#/bin/bash
module load python/anaconda3.2019.3
scriptDir=`pwd`
belle2_trace_dir=${scriptDir}/Belle2TracesforTazer/

ioratio=$1
iorate=$(( 125*ioratio ))

tpf=$2
numTasks=$3
cores_per_node=$4
nodes=$5
numCores=$((cores_per_node * nodes))
tazer_servers=$6
use_local_server=$7
# ${ioratio} ${tpf} ${numTasks} ${cores_per_node} ${nodes} ${tazer_server_nodes} ${use_local_server} ${tlimit} ${numCycles} ${readProbability} ${random_access} ${scalable} ${private_size} ${shared_mem} ${shared_size}
scalable=${11}
private_size=${12}
shared_mem=${13}
shared_size=${14}
bb=${15}
bb_size=${16}

echo "scalable:${scalable} private size:${private_size}MB shared:${shared_mem} sh size:${shared_size}MB"

#file_names="tazer.dat tazer1.dat tazer2.dat tazer3.dat tazer4.dat tazer5.dat tazer6.dat tazer7.dat tazer8.dat tazer9.dat tazer10.dat tazer11.dat tazer12.dat tazer13.dat tazer14.dat"

#file_paths="" 

#for file in $file_names; do
#    echo "TAZER0.1" > ${file}.meta.in
#    echo "type=input" >> ${file}.meta.in
#    echo "[server]" >> ${file}.meta.in
#    echo "host=${tazer_server}" >> ${file}.meta.in
#    echo "port=6024" >> ${file}.meta.in
#    echo "file=${TAZER_ROOT}/script/${file}" >> ${file}.meta.in
#done


coreIoRate=6.7 # this is derived from an actual belle2 run

if (( use_local_server < 3 )); then
    use_bounded_filelock=1
else
    use_bounded_filelock=0
fi
use_bounded_filelock=$bb
echo "use_local_server: ${use_local_server} use_bounded_filelock: ${use_bounded_filelock}"

cat <<EOT > tasks/mc_tazer.sh
#!/bin/bash
MY_HOSTNAME=\`hostname\`
ulimit -n 4096

TAZER_PATH=${TAZER_ROOT}/src/client
echo "\$JOBID"
TASKDIR="./"
mkdir -p \$TASKDIR\$JOBID
cp send_task_msg.py \$TASKDIR\$JOBID
cp ParseTazerOutput.py \$TASKDIR\$JOBID
cd \$TASKDIR\$JOBID

loop=\`python send_task_msg.py "6=\$MY_HOSTNAME=\$JOBID"\`
while [ "\$loop" == "0" ] ; do
    start=\$(date +%s)
    end=\$(date +%s)
    tdiff=\$(( end - start ))
    twait=\`shuf -i 10-30 -n 1\`
    while [ \$tdiff -le \$twait ]; do
        end=\$(date +%s)
        tdiff=\$(( end - start ))
        #echo "\$start \$end \$tdiff"
    done
    echo  "checking to start..."
    loop=\`python send_task_msg.py "6=\$MY_HOSTNAME=\$JOBID"\`
done;

N=1
expName="tazer"
taskType="tazer"
ioType="tazer"

t=\$(date +%s)
var_names="StartTime" && var_vals="\${t}" && var_times="\${t}"
var_names="\${var_names},N" && var_vals="\${var_vals},\${N}" && var_times="\${var_times},\${t}"
var_names="\${var_names},ExpName" && var_vals="\${var_vals},\${expName}" && var_times="\${var_times},\${t}"
var_names="\${var_names},TaskType" && var_vals="\${var_vals},\${taskType}" && var_times="\${var_times},\${t}"
var_names="\${var_names},Host" && var_vals="\${var_vals},\${MY_HOSTNAME}" && var_times="\${var_times},\${t}"
var_names="\${var_names},IOType" && var_vals="\${var_vals},\${ioType}" && var_times="\${var_times},\${t}"
var_names="\${var_names},Slot" && var_vals="\${var_vals},\${SLOT}" && var_times="\${var_times},\${t}"

WORKDIR=\`pwd\`
echo "### Working directory on ###"
echo \$WORKDIR



echo "### Running on ###"
hostname
tazer_lib=\${TAZER_PATH}/libclient.so 
out_dir=./
data_dir=./
mkdir -p \$out_dir
mkdir -p \$data_dir


module load gcc/8.1.0  

cd \$WORKDIR
echo "### Working directory on ###"
echo \$WORKDIR
ls

echo "### Creating tazer meta files"


compression=0
blocksize=\$(( 64*1024 )) #16777216


dset=\$(( \${MYID} / ${tpf} ))
t=\$(date +%s)
var_names="\${var_names},InputDataSet" && var_vals="\${var_vals},\${dset}" && var_times="\${var_times},\${t}"
# files="tazer.dat tazer2.dat tazer3.dat tazer4.dat tazer5.dat tazer6.dat tazer7.dat tazer8.dat tazer9.dat tazer10.dat"

file_names="tazer.dat tazer1.dat tazer2.dat tazer3.dat tazer4.dat tazer5.dat tazer6.dat tazer7.dat tazer8.dat tazer9.dat tazer10.dat tazer11.dat tazer12.dat tazer13.dat tazer14.dat"

file_paths="" 

for file in \$file_names; do
    echo "TAZER0.1" > \${file}.meta.in
    echo "type=input" >> \${file}.meta.in
    for server in `python ParseSlurmNodelist.py $tazer_servers`; do
        echo "[server]" >> \${file}.meta.in
        echo "host=\${server}" >> \${file}.meta.in
        echo "port=5001" >> \${file}.meta.in
        echo "file=${TAZER_ROOT}/script/\${file}" >> \${file}.meta.in
    done
done

for file in \$files; do
    infile=/files0/belo700/speedracer/test/to_remove/tazer/script/\${file} #assuming server is on seapearl
    if [ "${use_local_server}" == "0" ]; then
        server="130.20.68.151" 
        echo "\${server}:5101:\${compression}:0:0:\${blocksize}:\${infile}|\${server}:5201:\${compression}:0:0:${blocksize}:\${infile}|\${server}:5301:\${compression}:0:0:\${blocksize}:\${infile}|\${server}:5401:\${compression}:0:0:\${blocksize}:\${infile}|" | tee \$data_dir/\${file}.meta.in
    else
        server_list=""
        if [ "${use_local_server}" == "1" ] || [ "${use_local_server}" == "3" ]; then
            echo "TAZER0.1" > \$data_dir/\${file}.meta.in
            echo "type=input" >>\$data_dir/\${file}.meta.in
            for server in `python ParseSlurmNodelist.py $tazer_servers`; do
                echo "[server]" >> \$data_dir/\${file}.meta.in
                echo "host=${tazer_servers}" >>\$data_dir/\${file}.meta.in
                echo "port=5001" >> \$data_dir/\${file}.meta.in
                echo "file=\${infile}" >>\$data_dir/\${file}.meta.in
            done
        else
            for server in `python ParseSlurmNodelist.py $tazer_servers`; do
                server_list="\${server_list}\${server}.ibnet:5001:\${compression}:0:0:\${blocksize}:\${infile}|\${server}:5001:\${compression}:0:0:\${blocksize}:\${infile}|"
            done
            echo "\${server_list}" | tee \$data_dir/\${file}.meta.in
        fi
    fi

    echo "CHECK THIS OUT!!!!!!"
    cat \$data_dir/\${file}.meta.in
    file_paths="\$file_paths \$data_dir/\${file}.meta.in "
    sleep 1
done

echo "TAZER0.1" > \${out_dir}/\${JOBID}.root.meta.out
echo "type=output" >>\${out_dir}/\${JOBID}.root.meta.out
for server in `python ParseSlurmNodelist.py $tazer_servers`; do
    echo "[server]" >> \${out_dir}/\${JOBID}.root.meta.out
    echo "host=\${server}" >>\${out_dir}/\${JOBID}.root.meta.out
    echo "port=5001" >> \${out_dir}/\${JOBID}.root.meta.out
    #echo "file=output/\${JOBID}.root" >>\${out_dir}/\${JOBID}.root.meta.out
done

file_paths="\$file_paths  \$out_dir/\${JOBID}.root.meta.out"

echo "### Copying input files ###"
t=\$(date +%s)
var_names="\${var_names},StartInputTx" && var_vals="\${var_vals},\${t}" && var_times="\${var_times},\${t}"
t=\$(date +%s)
var_names="\${var_names},StopInputTx" && var_vals="\${var_vals},\${t}" && var_times="\${var_times},\${t}"


echo "## Running belle2 scalable tests##"
outfile=workload_sim_\${JOBID}.txt

t=\$(date +%s)
var_names="\${var_names},StartExp" && var_vals="\${var_vals},\${t}" && var_times="\${var_times},\${t}"

TAZER_SCALABLE_CACHE=${scalable}
TAZER_SCALABLE_CACHE_NUM_BLOCKS=${private_size}
TAZER_PRIVATE_MEM_CACHE_SIZE=$((private_size*1024*1024))
TAZER_SHARED_MEM_CACHE=${shared_mem}
TAZER_SHARED_MEM_CACHE_SIZE=$((shared_size*1024*1024))
TAZER_UMB_THRESHOLD=50
# TAZER_BB_CACHE_SIZE=\$(( 195*1024*1024*1024 ))
TAZER_BOUNDED_FILELOCK_CACHE_SIZE=$((bb_size*1024*1024))

echo "sizes: \${TAZER_PRIVATE_MEM_CACHE_SIZE} \${TAZER_SHARED_MEM_CACHE_SIZE} \${TAZER_BB_CACHE_SIZE} \${TAZER_BOUNDED_FILELOCK_CACHE_SIZE}"

TAZER_TRACE_HISTOGRAM=0
# TAZER_LIB_PATH=${TAZER_BUILD_DIR}build/src/client/libclient.so

echo `pwd`
time TAZER_PREFETCH=0 \
TAZER_SCALABLE_CACHE=\${TAZER_SCALABLE_CACHE} TAZER_SCALABLE_CACHE_NUM_BLOCKS=\${TAZER_SCALABLE_CACHE_NUM_BLOCKS} TAZER_PRIVATE_MEM_CACHE_SIZE=$((private_size*1024*1024)) \
TAZER_SHARED_MEM_CACHE=\${TAZER_SHARED_MEM_CACHE} TAZER_SHARED_MEM_CACHE_SIZE=\${TAZER_SHARED_MEM_CACHE_SIZE} \
TAZER_BOUNDED_FILELOCK_CACHE=${use_bounded_filelock} TAZER_BOUNDED_FILELOCK_CACHE_SIZE=\${TAZER_BOUNDED_FILELOCK_CACHE_SIZE} \
gdb -ex run --ex "thread apply all bt" --args env LD_PRELOAD=\${preload}:${TAZER_BUILD_DIR}src/client/libclient.so /files0/belo700/speedracer/test/tazer-bigflow-sim/workloadSim -f /files0/belo700/speedracer/test/to_remove/tazer/script/paper_experiments/exp4/OceaneTrace_\${dset}.txt -i ${coreIoRate} -m ".meta.in" -o ".meta.out" -t ${timelimit} >& \${out_dir}\${outfile}

t=\$(date +%s)
var_names="\${var_names},StopExp" && var_vals="\${var_vals},\${t}" && var_times="\${var_times},\${t}"

parsed=\`python ParseTazerOutput.py \${out_dir}\${outfile}\`
tmp_names=\`echo "\$parsed" | grep -oP '(?<=labels:).*'\` 
tmp_vals=\`echo "\$parsed" | grep -oP '(?<=vals:).*'\` 
var_names="\${var_names},\${tmp_names}" && var_vals="\${var_vals},\${tmp_vals}"

#rm -r \$data_dir

t=\$(date +%s)
var_names="\${var_names},FinishedTime" && var_vals="\${var_vals},\${t}" && var_times="\${var_times},\${t}"
python send_task_msg.py "2=\$MY_HOSTNAME=\$JOBID=\$JOBID;\$var_names;\$var_vals"
python send_task_msg.py "1=\$MY_HOSTNAME=\$JOBID"
wait
EOT

# done

echo "creating task list "
touch CurTasks.dat
for i in `seq 0 $(( numTasks-1 ))`; do
# echo "${iorate}_${tpf}_$(( i/tpf )).sh" >> CurTasks.dat
#echo "${iorate}_${tpf}.sh" >> CurTasks.dat
echo "mc_tazer.sh" >> CurTasks.dat

# for i in `seq 1 ${cores_per_node}`; do
# echo "${iorate}_${tpf}.sh" >> CurTasks.dat
done
module unload python/anaconda3.2019.3
