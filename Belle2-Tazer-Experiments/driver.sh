#!/bin/bash
module purge
module load gcc/8.1.0
#module unload python/anaconda3.2019.3

#---------------CHANGE ME!!!!---------------------
# this should point to the install dir for tazer
export TAZER_ROOT=/people/mutl832/tazer-install-dir/
export TAZER_BUILD_DIR=/people/mutl832/tazer_november/build/
TAZER_DATA_DIR=/files0/mutl832/tzr/
WORKLOADSIM_PATH=${HOME}/tazer-bigflowsim/workloadSim
#-------------------------------------------------


#------- find directory this script is located----------

# SOURCE="${BASH_SOURCE}"
# dirname "${BASH_SOURCE}"
# echo "S: ${SOURCE}"
# while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
#     DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
#     echo "D: ${DIR}"
#     SOURCE="$(readlink "$SOURCE")"
#     [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
#     echo "S: ${SOURCE}"
# done


SCRIPT_NAME=$(basename $(test -L "$0" && readlink "$0" || echo "$0"));
scripts_dir=$(cd $(dirname "$0") && pwd);
# scripts_dir="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
#------------------------------------------------------

#----------directory this script is executed---------------
exp_base_dir=`pwd`
data_dir=/files0/mutl832/tzr/
#----------------------------------------------------------

echo "scripts_dir=${scripts_dir}"
echo "exp_base_dir=${exp_base_dir}"
echo "TAZER_ROOT=${TAZER_ROOT}"
export TAZER_EXP_DIR=${exp_base_dir}



# ------------- experiment parameters: -----------
cores_per_node=$1
nodes=$2
numRounds=1
use_local_server=1 #0=no local servers, 1=local server over ethernet, 2= local server over ethernet and IB, 3 forwarding server over ethernet, 4 forwarding server over ether + IB
numTasks=$(( cores_per_node * nodes * numRounds ))
echo "Number of tasks = ${numTasks}"
tlimit=0 #time limit for tasks in seconds. Set to 0 for no limit.  
numCycles=1 #1=linear, {1,4,16}
readProbability=1 # {1, 0.25}
random_access=""
seapearl_server=0
#-------------------------------------------------

#----------scalable parameters ---------
scalable=$6
private_size=$3
shared_mem=1
shared_size=$4
bb=1
bb_size=$5
#---------------------------------------


task_server_port=5001

#for ioratio in  1 4 16; do #with respect to 125MB/s
#    for tpf in 4 16 128; do
for ioratio in 1; do
    for tpf in 1; do
        rm -r /files0/${USER}/tazer_cache/ #cleanup from previous experiment
        rm -r ${cores_per_node}clients_${nodes}cores_${scalable}_${shared_mem}_${bb} #$((ioratio*125))MBs_io_${tpf}_tpf
        mkdir ${cores_per_node}clients_${nodes}cores_${scalable}_${shared_mem}_${bb} #$((ioratio*125))MBs_io_${tpf}_tpf 
        cd ${cores_per_node}clients_${nodes}cores_${scalable}_${shared_mem}_${bb} #$((ioratio*125))MBs_io_${tpf}_tpf
        cp -r ${scripts_dir}/* .
        #cp -r ${scripts_dir}/*.py .
        #cp -r ${scripts_dir}/Makefile .
        #cp -r ${scripts_dir}/src/ .
        #cp -r ${scripts_dir}/tasks/ .
        #cp -r ${scripts_dir}/inc/ .
        #cp -r ${scripts_dir}/tazer-workflow-sim/ .
        #cp -r ${scripts_dir}/tzr/ .
        #--------------- build utils ---------------------
        make 
        # cd tazer-workflow-sim
        # make
        # cp workloadSim ..
        # cd ..
        # cp ${WORKLOADSIM_PATH} ./
        cp /people/mutl832/tazer-bigflowsim/workloadSim ./
        #-------------------------------------------------
        chmod +x *.sh
        if [ "${use_local_server}" != "0" ]; then
            echo "launching tazer servers"
            if [ "${seapearl_server}" = "1" ]; then
                echo "seapearl server"
                tazer_server_task_id=`ssh -f seapearl "cd /home/mutl832/TazerServerOutput/ ; ulimit -n 4096; nohup /home/mutl832/projects/tazer-merged/build/src/server/server 5001 > /home/mutl832/TazerServerOutput/server.log 2>&1 &"` 
                sleep 5
                tazer_server_nodes="[seapearl]"
            else
                echo "bluesky server"
                tazer_server_task_id=$(sbatch -A chess --exclude=node25,node33,node15 -N1 --parsable ./launch_tazer_server.sh $use_local_server $TAZER_DATA_DIR) #node33 infiniband is not working?
                echo "tazer server task id: $tazer_server_task_id"
                tazer_server_nodes=`squeue -j ${tazer_server_task_id} -h -o "%N"`
                while [ -z "$tazer_server_nodes" ]; do
                    sleep 1
                    tazer_server_nodes=`squeue -j ${tazer_server_task_id} -h -o "%N"`
                done
                echo "TAZER SERVER NODES: $tazer_server_nodes"
            fi
        else
            tazer_server_nodes="[bluesky]" 
        fi

        echo "creating tasks"
	#./create_belle2_mc_task.sh ${ioratio} ${tpf} ${numTasks} ${cores_per_node} ${nodes} ${tazer_server_nodes} ${use_local_server} ${tlimit} ${numCycles} ${readProbability} ${random_access}
	#./create_belle2_mc_task_local.sh ${ioratio} ${tpf} ${numTasks} ${cores_per_node} ${nodes} ${tazer_server_nodes} ${use_local_server} ${tlimit} ${numCycles} ${readProbability} ${random_access}
	#./create_synthetic_belle2_mc_task_optimal.sh ${ioratio} ${tpf} ${numTasks} ${cores_per_node} ${nodes} ${tazer_server_nodes} ${use_local_server} ${tlimit} ${numCycles} ${readProbability} ${random_access}
	#./create_synthetic_belle2_mc_task.sh ${ioratio} ${tpf} ${numTasks} ${cores_per_node} ${nodes} ${tazer_server_nodes} ${use_local_server} ${tlimit} ${numCycles} ${readProbability} ${random_access}#
        #./create_tazer_cp_task.sh ${ioratio} ${tpf} ${numTasks} ${cores_per_node} ${nodes} ${tazer_server_nodes} ${use_local_server} ${tlimit} ${numCycles} ${readProbability} ${random_access}
        #./create_optimal_task.sh ${ioratio} ${tpf} ${numTasks} ${cores_per_node} ${nodes} ${tazer_server_nodes} ${use_local_server} ${tlimit} ${numCycles} ${readProbability} ${random_access}
      #./create_task.sh ${ioratio} ${tpf} ${numTasks} ${cores_per_node} ${nodes} ${tazer_server_nodes} ${use_local_server} ${tlimit} ${numCycles} ${readProbability} ${random_access}
       ./create_scalable_belle2_task.sh ${ioratio} ${tpf} ${numTasks} ${cores_per_node} ${nodes} ${tazer_server_nodes} ${use_local_server} ${tlimit} ${numCycles} ${readProbability} ${random_access} ${scalable} ${private_size} ${shared_mem} ${shared_size}
       #./create_scalable_exp4_task.sh ${ioratio} ${tpf} ${numTasks} ${cores_per_node} ${nodes} ${tazer_server_nodes} ${use_local_server} ${tlimit} ${numCycles} ${readProbability} ${random_access} ${scalable} ${private_size} ${shared_mem} ${shared_size} ${bb} ${bb_size}
        echo "launching task server"
        get_task_id=`sbatch -A chess --parsable ./get_task.sh ${task_server_port}`
        get_task_node=`squeue -j ${get_task_id} -h -o "%N"`
        while [ -z "$get_task_node" ]; do
        sleep 1
        get_task_node=`squeue -j ${get_task_id} -h -o "%N"`
        done
        sleep 15 #wait for get_task to start server to initialize...
        workers_task_id=`sbatch -A chess --exclude=node33,node04,node25 --parsable -d after:${get_task_id} -N ${nodes} ./launch_workers.sh ${get_task_node} ${task_server_port} ${cores_per_node} ${nodes} ${numRounds}`
        
        salloc -A chess -N1 -d afterany:${workers_task_id} scancel ${get_task_id}
	if [ "${use_local_server}" != "0" ]; then
            echo "closing servers"
            for server in `python ParseSlurmNodelist.py $tazer_server_nodes`; do 
                echo "closing $server"
                if [ "${use_local_server}" == "2" ]; then
                    ${TAZER_BUILD_DIR}/test/CloseServer ${server}.ibnet 5001
                fi
               ${TAZER_BUILD_DIR}/test/CloseServer $server 5001
            done
        fi
        echo "${get_task_id} ${get_task_node}"
        scancel ${get_task_id}
        #./create_plots.sh
        # pkill tazer #hmmm this seems suspicious...
        cd ..
    done
done
