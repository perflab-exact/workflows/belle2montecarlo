# -*- coding: utf-8 -*-
# Descriptor: MC15rd_b (JIRA: BIIDP-6097) - Phase: eph3 mixed (Run Dependent BG)

#############################################################
# Steering file for official MC production of signal samples
#
# April 2021 - Belle II Collaboration
#############################################################

import basf2 as b2
import generators as ge
import simulation as si
import L1trigger as l1
import reconstruction as re
import mdst as mdst
import glob as glob

# background (collision) files
bg = glob.glob('/qfs/projects/oddite/stru821/Belle2/data/simulation_background/BGx1/*.root')

# create path
main = b2.create_path()

# specify number of events to be generated
main.add_module("EventInfoSetter", expList=0, runList=0, evtNumList=10000)

# events generator
ge.add_evtgen_generator(path=main, finalstate='mixed')

# detector simulation
si.add_simulation(main, bkgfiles=bg)

# reconstruction
re.add_reconstruction(main)

# Finally add mdst output
mdst.add_mdst_output(main, additionalBranches=['EventExtraInfo'], filename="mdst.root")

b2.process(main)