Belle II: Variants
=============================================================================

1. Vanilla
2. Vanilla with TAZeR
3. BigFlowSim with real traces
4. BigFlowSim with synthetic (exploratory) traces

This focuses on (1) and (2).


Belle II: Monte Carlo Simulation (Vanilla)
=============================================================================

## Data for background overlay
## Github

- https://github.com/belle2/externals
- https://github.com/belle2/tools
- https://github.com/belle2/versioning
- https://github.com/belle2/basf2


## Prerequisite packages for CentOS, RHEL starting with version 8 (junction)

- PACKAGES="binutils gcc gcc-c++ git make patch perl-devel python2 subversion tar gzip bzip2 bzip2-devel xz unzip wget libpng-devel libX11-devel  libXext-devel libXpm-devel libXft-devel ncurses-devel openssl-devel readline-devel which lsof man-db texinfo"

- OPTIONALS="tk-devel tcl-devel mesa-libGL-devel flex bison"


## Installing the externals -- using binaries

1. Set up the tools: `source tools/b2setup`
2. Check for prerequisites: `b2install-prepare`
3. Install externals: `b2install-externals v01-10-02`
4. Install release: `b2install-release release-06-01-08`


Belle II: Monte Carlo Simulation
=============================================================================

## Data for background overlay

| Location locally | copied from |
| ---- | ---- |
| collision background | `rsync -a kekcc:/group/belle2/dataprod/MC/ecl_leakageCorrections/BGOverlay/bucket36/sub00/beambg_000018* .` |
| BGx1 | `rsync -a kekcc:/group/belle2/dataprod/BGOverlay/nominal_phase3/release-06-00-05/overlay/BGx1/set0/*1.root .` | 
| BGx3 | `rsync -a kekcc:/group/belle2/dataprod/BGOverlay/nominal_phase3/release-06-00-05/overlay/BGx3/set0/*1.root .` |


## Steering file for running simulation production

Copying inline from: <https://stash.desy.de/projects/B2P/repos/mc/browse/MC15/MC15rd_b/release-06-01-08/DB00002284/4S/bucket36/steering_mixed_MC15rd_b_exp0026_bucket36_eph3_4S.py> (internal)
(specific version is checked into this repo)

```python
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Descriptor: MC15rd_b (JIRA: BIIDP-6097) - Phase: eph3 mixed (Run Dependent BG)

#############################################################
# Steering file for official MC production of signal samples
#
# April 2021 - Belle II Collaboration
#############################################################

import basf2 as b2
import generators as ge
import simulation as si
import L1trigger as l1
import reconstruction as re
import mdst as mdst
import glob as glob

# set database conditions (in addition to default)
b2.conditions.append_globaltag('mc_production_MC15rd_a_exp26_bucket36')
b2.conditions.append_globaltag('data_reprocessing_prompt')
b2.conditions.append_globaltag('online')


# background (collision) files
bg = glob.glob('./*.root')
#if running locally
bg_local = glob.glob("./sub00/*.root")


# create path
main = b2.create_path()

# specify number of events to be generated
main.add_module("EventInfoSetter", expList=0, runList=0, evtNumList=10000)

# events generator
ge.add_evtgen_generator(path=main, finalstate='mixed')

# detector simulation
si.add_simulation(main, bkgfiles=bg)

# reconstruction
re.add_reconstruction(main)

# Finally add mdst output
mdst.add_mdst_output(main, additionalBranches=['EventExtraInfo'], filename="mdst.root")

# process events and print call statistics
b2.process(main)
print(b2.statistics)
```


Example run on junction
=============================================================================

```bash
# cd /qfs/projects/oddite/stru821/Belle2/
# . belle2_env/bin/activate
# . tools/b2setup release-06-01-08
# cd workflows

clone https://gitlab.pnnl.gov/perf-lab/workflows/belle2 belle2 && cd belle2

. /qfs/projects/oddite/stru821/Belle2/tools/b2setup release-06-01-08

basf2 -n10 ./steering_mixed_MC15rd_b_exp0026_bucket36_eph3_4S.py
```
